import graphene

class Cat(graphene.ObjectType):
    catname = graphene.String()
    cuteness = graphene.String()

class Query(graphene.ObjectType):
    cat = graphene.Field(Cat)

    def resolve_cat(self, info, **kwargs):
        return 'hi im soda im baby will you take care of me please?'

schema = graphene.Schema(query=Query)
