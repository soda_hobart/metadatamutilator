from Mods import MetadataComponent
import attr

@attr.s
class authorityAttributeGroup(MetadataComponent):
    """MODS attributes authority, authorityURI, valueURI"""
    # NOTE:  Belonging to ABC class MetadataComponent should
    # prevent this class from being instantiated on its own,
    # only when it is part of the inheritance structure of
    # a MODS element/subelement class will it have the
    # serialize() and validate() methods required by the
    # interface declared in MetadataComponent!!

    authority = attr.ib(
        type=str,
        default=None,
        validator=attr.validators.instance_of((str, None))
    )
    authorityURI = attr.ib(default=None)
    @authorityURI.validator
    def _check_anyURI(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Must be str")
        # TODO: get a stuntin' regex to validate URI/also create
        # URI datatype maybe?

    valueURI = attr.ib(default=None)
    @valueURI.validator
    def _check_anyURI(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Must be str")

@attr.s
class languageAttributeGroup(MetadataComponent):
    """MODS attributes lang, xml_lang, script, and transliteration"""

    lang = attr.ib(
        type=str,
        default=None,
        validator=attr.validators.instance_of((str, None))
    )

    xml_lang = attr.ib(default=None)
    # ISO 639-1 Language Codes
    
    script = attr.ib(
        type=str,
        default=None,
        validator=attr.validators.instance_of((str, None))
    )

    transliteration = attr.ib(
        type=str,
        default=None,
        validator=attr.validators.instance_of((str, None))
    )

@attr.s
class altFormatAttributeGroup(MetadataComponent):
    """MODS attributes altFormat and contentType"""

    altFormat = attr.ib(default=None)
    @altFormat.validator
    def _check_anyURI(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("must be str")
    # TODO figure out how to validate xs:anyURI...

    contentType = attr.ib(
        type=str,
        default=None,
        validator=attr.validators.instance_of((str, None))
    )

