from Mods import MetadataComponent
import attr

@attr.s
class authorityAttributeGroup(MetadataComponent):
    """MODS attributes authority, authorityURI, valueURI"""
    # NOTE:  Belonging to ABC class MetadataComponent should
    # prevent this class from being instantiated on its own,
    # only when it is part of the inheritance structure of
    # a MODS element/subelement class will it have the
    # serialize() and validate() methods required by the
    # interface declared in MetadataComponent!!

    authority = attr.ib(type=str,default=None)
    @authority.validator
    def _check_authority(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Must be str")
        
    authorityURI = attr.ib(type=str,default=None)
    @authority.validator
    def _check_anyURI(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Must be str")

    valueURI = attr.ib(default=None)
    @valueURI.validator
    def _check_anyURI(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Must be str")

@attr.s
class languageAttributeGroup(MetadataComponent):
    """MODS attributes lang, xml_lang, script, and transliteration"""

    lang = attr.ib(type=str,default=None)
    @lang.validator
    def _check_lang(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Must be str")

    xml_lang = attr.ib(default=None)
    # ISO 639-1 Language Codes

    script = attr.ib(type=str, default=None)
    @script.validator
    def _check_script(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Must be str")

    transliteration = attr.ib(type=str,default=None)
    @transliteration.validator
    def _check_transliteration(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Must be str")
    

@attr.s
class altFormatAttributeGroup(MetadataComponent):
    """MODS attributes altFormat and contentType"""

    altFormat = attr.ib(default=None)
    @altFormat.validator
    def _check_anyURI(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("must be str")
    # TODO figure out how to validate xs:anyURI...

    contentType = attr.ib(type=str,default=None)
    @contentType.validator
    def _check_contentType(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("must be str")
        

