from abc import ABCMeta, abstractmethod
from MetadataObj import MetadataObj
import attr
import click

@attr.s
class ModsObj(object):
    """A MODS record, collection, or element"""

    parent = attr.ib(default='None')
                
    @classmethod
    def from_xml(cls, etree_obj):
        """Factory for instantiating and initializing MODS elements from a MODS XML document parsed by lxml etree"""
        pass

    def serialize(self):
        pass

    def validate(self):
        pass

    def clone(self):
        pass

    def __iter__(self):
        pass

    def __str__(self):
        return str(attr.asdict(self).items())

@attr.s
class titleInfo(ModsObj):

    children = attr.ib(factory=list)

    ID = attr.ib(default=None)
    modstype = attr.ib(default=None)

    @classmethod
    def constructor(cls, ID=None, modstype=None):
        instance = cls()
        instance.parent = parent
        instance.ID = ID
        instance.modstype = modstype
        return instance

    @classmethod
    @click.command()
    @click.option('--ID,')
    @click.option('--modstype')
    def build(cls):
        instance = cls()
        instance.ID = ID
        instance.modstype = modstype
        return instance

    def spawn_child(self, child_callback):
        print(type)

    def spawn_parent(self, *args, **kwargs):
        pass
        
    
if __name__ == '__main__':
    t = titleInfo.build()
