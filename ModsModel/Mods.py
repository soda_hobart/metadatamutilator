import abc
import attr
from lxml import etree
import copy
import json
from io import StringIO

# Classes defined in this file:
# Abstract Base Classes:  MetadataComposite, MetadataComponent
# Generic MODS composite object: ModsObj
# MODS classes: Mods, ModsCollection


class MetadataComposite(metaclass=abc.ABCMeta):
    """Declares interface for a metadata composite object,
ie. anything that has child elements"""

    @abc.abstractmethod
    def serialize(self):
        """Serializes the composite object to XML or another
data serialization format"""
        pass


    @abc.abstractmethod
    def validate(self):
        """Applies validators to relevant data members"""
        pass

    #@abc.abstractclassmethod
    #def mods_name(cls):
    #    pass

# How about a method that returns the values used for
# construction/initialization?

class MetadataComponent(metaclass=abc.ABCMeta):
    """Declares interface for a primitive metadata element, in
terms of MODS this means that it is a bottom-level element
that includes text; or, an attribute group"""

    @abc.abstractmethod
    def serialize(self):
        """Serializes the primitive object to XML (or another data 
serialization format)"""
        pass

    @abc.abstractmethod
    def validate(self):
        """Applies validators to relevant data members"""
        pass

@attr.s
class ModsObj(MetadataComposite):
    """A single MODS record, MODS collection, top level
element, or subelement that has subelements"""

    resource_uri = attr.ib(default=None)
    parent_resource_uri = attr.ib(default=None)

    def present_subelements(self):
        """Returns list of mods_name() of all elements present"""
        output = []
        def recurse(elmt, output):
            try:
                if elmt.subelements:
                    subelmts = [i.__class__.mods_name() for i in elmt.subelements]
                    output += subelmts
                    for i in elmt.subelements:
                        recurse(i, output)
            except AttributeError:
                print('{} has no subelements, exception ignored'.format(elmt.__class__))
        recurse(self, output)
        return output


    def iter_all(self):
        """Returns list of all subelements in tree"""
        output = [self]
        def recurse_iter_all(element):
            try:
                for i in element.subelements:
                    output.append(i)
                    recurse_iter_all(i)
            except AttributeError:
                print('Element has no subelements, exception ignored')
        recurse_iter_all(self)
        return output
            
        

    def serialize(self, output='xml', recurse=True):
        """Returns an lxml etree Element instance"""
        # Loop through data members to serialize as XML
        # Is order of attributes important?
        attributes = attr.asdict(self,
                                 filter=attr.filters.exclude(list))
        if output == 'json':
            attributes['datatype'] = type(self).mods_name()
            data_obj = StringIO()
            if recurse:
                attributes['subelements'] = [json.loads(i.serialize(output='json', recurse=True)) for i in self.subelements]
            data = json.dump(attributes, data_obj)
                #io = StringIO()
                #data = json.dump(attributes, io)
            return data_obj.getvalue()
        if output == 'xml':
            # li'l NS fix
            xml = etree.Element(self.__class__.mods_name())
            for key, value in attributes.items():
                # nother li'l NS fix
                if key == 'modsType':
                    attributes['type'] = attributes.pop(key)
                if value and key != 'text':
                    xml.set(key, value)
                if key == 'text':
                    xml.text = value
            print(etree.tostring(xml))
            if recurse == True:
                try:
                    if self.subelements:
                        for i in self.subelements:
                            xml.append(i.serialize())
                except AttributeError:
                    pass
            #return etree.tostring(xml, pretty_print=True)
            return xml

    @classmethod
    def from_xml(cls):
        pass

    @classmethod
    def from_json(cls, json_str):
        data = json.loads(json_str)
        element = cls.json_factory(data)
        return element
        
    @classmethod
    def json_factory(cls, data):
        if data['datatype'] == cls.mods_name():
            kwargs = {k: v for k, v in data.items() if k not in ['datatype', 'subelements']}
            element = cls(**kwargs)
            if data['subelements']:
                for i in data['subelements']:
                    element.add_subelement(json_factory(i))
        return element

    

                     
    def validate(self):
        # Loop through collection of validators
        # ...or just use attrs.  Thanks attrs!
        attr.validate(self)
        if len(self.subelements) > 0:
            for subelement in self.subelements:
                subelement.validate()

    def add_subelement(self, callback, recurse=True):
        """Accepts a callback (constructor, copy, from_xml factory) to populate subelements"""
        self.subelements.append(callback)

    def copy(self, target=None, verbose=True):
        if target:
            try:
                target.subelements.append(self)
                if verbose:
                    print('{} added to subelements of {}'.format(
                        type(self).__name__, type(target).__name__))
            except AttributeError:
                print('{} cannot accept subelements!'.format(
                    type(target).__name__))
        else:
            return self

    def move(self, target, verbose=True):
        new_self = copy.deepcopy(self)
        new_self.copy(target=target, verbose=verbose)
        del self
        #if verbose:
        #    print('{} copied to subelements of {}'.format(
        #        self.__name__, target.__name__))
            
        
    

@attr.s    
class Mods(ModsObj):
    """A single MODS record"""

    @classmethod
    def mods_name(cls):
        return 'mods'


    ID = attr.ib(default=None)
    version = attr.ib(default='3.7')

    # TODO MODS record must have at least 1 subelement
    subelements = attr.ib(factory=list)
    xmlns = attr.ib(default='http://www.loc.gov/mods/v3')

@attr.s
class ModsCollection(ModsObj):
    """A MODS collection"""
    # I don't anticipate using this class very much
    
    subelements = attr.ib(factory=list)

@attr.s
class ModsElementAggregation(ModsObj):
    """Not a MODS class, rather an aggregation of MODS fragments"""
    @classmethod
    def mods_name(cls):
        return 'modsElementAggregation'
    
    subelements = attr.ib(factory=list)
    
        
        
    




