import attr
from .AttributeGroup import languageAttributeGroup, authorityAttributeGroup
from lxml import etree
import copy
import json
from io import StringIO

@attr.s
class stringPlusLanguage(languageAttributeGroup):
    resource_uri = attr.ib(default=None)
    parent_resource_uri = attr.ib(default=None)
    text = attr.ib(
        type=str,
        default=None,
        validator=attr.validators.instance_of(str)
    )

    def validate(self):
        attr.validate(self)

    def serialize(self, output='xml', recurse=True):
        attributes = attr.asdict(self,
                                 filter=attr.filters.exclude(list))
        if output == 'json':
            attributes['datatype'] = type(self).mods_name()
            data = json.dumps(attributes)
            data_obj = StringIO()
            data_obj.write(data)
            if recurse == True:
                return data_obj.getvalue()
            else:
                return data
        if output == 'xml':
            xml = etree.Element(self.__class__.mods_name())
            for key, value in attributes.items():
                if value and key != 'text':
                    xml.set(key, value)
                if key == 'text':
                    xml.text = value
            print(etree.tostring(xml))
            if recurse == True:
                try:
                    if self.subelements:
                        for i in self.subelements:
                            xml.append(i.serialize())
                except AttributeError:
                    pass
            return xml

    @classmethod
    def from_xml(cls):
        pass

    @classmethod
    def from_json(cls, json_str):
        data = json.loads(json_str)
        if data['datatype'] == cls.mods_name():
            kwargs = {k: v for k, v in data.items() if k != 'datatype'}
            subelement = cls(**kwargs)
            return element

    def copy(self, target=None, verbose=True):
        if target:
            try:
                target.subelements.append(self)
                if verbose:
                    print('{} added to subelements of {}'.format(
                        type(self).__name__, type(target).__name__))
            except AttributeError:
                print('{} cannot accept subelements!'.format(
                    type(target).__name__))
        else:
            return self

    def move(self, target, verbose=True):
        new_self = copy.deepcopy(self)
        new_self.copy(target=target, verbose=verbose)
        del self
        #if verbose:
        #    print('{} copied to subelements of {}'.format(
        #        self.__name__, target.__name__))

@attr.s
class stringPlusLanguagePlusAuthority(stringPlusLanguage, authorityAttributeGroup):
    pass # :)

@attr.s
class stringPlusLanguagePlusSupplied(stringPlusLanguage):
    @classmethod
    def supplied_vals(cls):
        supplied_vals = [True, False]
        return supplied_vals    
    supplied = attr.ib(default=None)
    @supplied.validator
    def _check_supplied(self, attribute, value):
        if value != None and value != 'yes':
            raise ValueError("If set, can only have value 'yes'")
