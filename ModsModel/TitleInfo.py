import attr
from .Mods import ModsObj
import re
from .AttributeGroup import altFormatAttributeGroup, authorityAttributeGroup, languageAttributeGroup
from .StringDefinitions import stringPlusLanguage 
import json


def builder(data):
    mappings = {'titleInfo': TitleInfo,
                'title': Title,
                'subTitle': SubTitle
                }
    element = None
    if data['datatype'] in mappings.keys():
        kwargs = {key: value for key, value in data.items() if key not in ['datatype', 'subelements']}
        element = mappings[data['datatype']](**kwargs)
    try:
        if data['subelements']:
            for i in data['subelements']:
                element.add_subelement(builder(i))
    except KeyError:
        print('no subelements, exception ignored')
    return element
    


@attr.s
class TitleInfo(ModsObj, authorityAttributeGroup, altFormatAttributeGroup, languageAttributeGroup):
    """Title Information--contains subelements Title, SubTitle, PartNumber, PartName, and NonSort"""

    @classmethod
    def mods_name(cls):
        return 'titleInfo'

    # These are the acceptable values for mods_type; needs to
    # be excluded from attr.asdict()
    @classmethod
    def modsType_vals(cls):
        modsType_vals = ['abbreviated', 'translated',
                          'alternative', 'uniform']
        return modsType_vals
    modsType = attr.ib(default=None)
    @modsType.validator
    def _check_modsType(self, attribute, value):
        if value != None and value not in TitleInfo.modsType_vals():
            raise ValueError(
                "Value must be type None or str: {}".format(
                    TitleInfo.modsType_vals()
                )
            )

    otherType = attr.ib(type=str,default=None)
    @otherType.validator
    def _check_otherType(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Needs to be str")

    # Not totally sure about using the bools, maybe just
    # "yes" would be better?
    @classmethod
    def supplied_vals(cls):
        supplied_vals = [True, False]
        return supplied_vals    
    supplied = attr.ib(default=None)
    @supplied.validator
    def _check_supplied(self, attribute, value):
        if value != None and value != 'yes':
            raise ValueError("If set, can only have value 'yes'")

    # Honestly, I have no idea what the heck this one does
    altRepGroup = attr.ib(type=str, default=None)
    @altRepGroup.validator
    def _check_altRepGroup(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Needs to be str")


    # as with altRepGroup, I'm a little hazy on this one
    # I think they are both used in creating aggregations
    nameTitleGroup = attr.ib(type=str,default=None)
    @nameTitleGroup.validator
    def _check_nameTitleGroup(self, attribute, value):
        if value != None and type(value) != str:
            raise TypeError("Needs to be str")

    usage = attr.ib(type=str,default=None)
    @usage.validator
    def _check_usage(self, attribute, value):
        if value != None and value == 'primary':
            raise ValueError("Needs to be None or str 'primary'")
    # This one could be useful for building the search indexes
    # http://www.datypic.com/sc/xsd/t-xsd_ID.html
    # Must be an NCName, must start with char or _
    ID = attr.ib(default=None)
    @ID.validator
    def _check_ID(self, attribute, value):
        if not re.fullmatch("^(_|\w)+[\w\.-]*", str(value)):
            raise ValueError(
                "Must only contain letters, numbers, '.', '_'"
                )

    # TODO: xlink:simpleLink

    #SubElements
    @classmethod
    def subelement_types(cls):
        subelement_types = [Title, SubTitle, PartNumber,
                            PartName, NonSort]
        return subelement_types
    subelements = attr.ib(factory=list)
    # this validator allows self.subelements to be empty
    # but the MODS record (Mods) validator call will be
    # decorated to require at least one subelement
    @subelements.validator
    def _check_subelements(self, attribute, value):
        for i in value:
            if type(i) != None and type(i) not in TitleInfo.subelement_types():
                raise TypeError("Datatype must be: {}".format(
                    [i.__name__ for i in TitleInfo.subelement_types()])
                )
            
# Title Info Subelements

@attr.s
class Title(stringPlusLanguage):
    """MODS Title"""

    @classmethod
    def mods_name(cls):
        return "title"

@attr.s
class SubTitle(stringPlusLanguage):
    """MODS SubTitle"""

    @classmethod
    def mods_name(cls):
        return "subTitle"

@attr.s
class PartNumber(stringPlusLanguage):

    @classmethod
    def mods_name(cls):
        return "partNumber"

@attr.s
class PartName(stringPlusLanguage):

    @classmethod
    def mods_name(cls):
        return "partName"

@attr.s
class NonSort:
    pass #this one's a clever piece of scum

if __name__ == '__main__':
    t = TitleInfo()
