from abc import ABCMeta, abstractmethod

class MetadataObj(metaclass=ABCMeta):
    """A generic object representing a metadata record or field"""

    @classmethod
    @abstractmethod
    def build(cls):
        raise NotImplementedError

    @abstractmethod
    def serialize(self):
        raise NotImplementedError

    @abstractmethod
    def validate(self):
        raise NotImplementedError

    @abstractmethod
    def __iter__(self):
        raise NotImplementedError

    @abstractmethod
    def __str__(self):
        raise NotImplementedError
