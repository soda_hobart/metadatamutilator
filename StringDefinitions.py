import attr
from AttributeGroup import languageAttributeGroup, authorityAttributeGroup
from lxml import etree

@attr.s
class stringPlusLanguage(languageAttributeGroup):
    text = attr.ib(
        type=str,
        default=None,
        validator=attr.validators.instance_of(str)
    )

    def validate(self):
        attr.validate(self)

    def serialize(self, output='xml', recurse=True):
        attributes = attr.asdict(self,
                                 filter=attr.filters.exclude(list))
        if output == 'xml':
            xml = etree.Element(type(self).__name__)
            for key, value in attributes.items():
                if value and key != 'text':
                    xml.set(key, value)
                if key == 'text':
                    xml.text = value
            print(etree.tostring(xml))
            if recurse == True:
                try:
                    if self.subelements:
                        for i in self.subelements:
                            xml.append(i.serialize())
                except AttributeError:
                    pass
            return xml

@attr.s
class stringPlusLanguagePlusAuthority(stringPlusLanguage, authorityAttributeGroup):
    pass # :)

@attr.s
class stringPlusLanguagePlusSupplied(stringPlusLanguage):
    @classmethod
    def supplied_vals(cls):
        supplied_vals = [True, False]
        return supplied_vals    
    supplied = attr.ib(default=None)
    @supplied.validator
    def _check_supplied(self, attribute, value):
        if value != None and value != 'yes':
            raise ValueError("If set, can only have value 'yes'")
