from ModsModel import Mods, TitleInfo, AttributeGroup, StringDefinitions
import json

mods_root = {'mods': Mods.Mods}
top_level = {'titleInfo': TitleInfo.TitleInfo}

if __name__ == '__main__':
    book = Mods.Mods()
    book.add_subelement(TitleInfo.TitleInfo(authority='sammy'))
    book.subelements[0].add_subelement(TitleInfo.Title(text='Friendliest Pet'))
    book.subelements[0].add_subelement(TitleInfo.SubTitle(text='The Soda Hobart Story'))

    j = book.serialize(output='json')
