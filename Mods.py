import abc
import attr
from lxml import etree

class MetadataComposite(metaclass=abc.ABCMeta):
    """Declares interface for a metadata composite object,
ie. anything that has child elements"""

    @abc.abstractmethod
    def serialize(self):
        """Serializes the composite object to XML or another
data serialization format"""
        pass


    @abc.abstractmethod
    def validate(self):
        """Applies validators to relevant data members"""
        pass

# How about a method that returns the values used for
# construction/initialization?

class MetadataComponent(metaclass=abc.ABCMeta):
    """Declares interface for a primitive metadata element, in
terms of MODS this means that it is a bottom-level element
that includes text; or, an attribute group"""

    @abc.abstractmethod
    def serialize(self):
        """Serializes the primitive object to XML (or another data 
serialization format)"""
        pass

    @abc.abstractmethod
    def validate(self):
        """Applies validators to relevant data members"""
        pass

@attr.s
class ModsObj(MetadataComposite):
    """A single MODS record, MODS collection, top level
element, or subelement that has subelements"""

    def serialize(self, output='xml', recurse=True):
        """Returns an lxml etree Element instance"""
        # Loop through data members to serialize as XML
        # Is order of attributes important?
        attributes = attr.asdict(self,
                                 filter=attr.filters.exclude(list))
        if output == 'xml':
            xml = etree.Element(type(self).__name__)
            for key, value in attributes.items():
                if value and key != 'text':
                    xml.set(key, value)
                if key == 'text':
                    xml.text = value
            print(etree.tostring(xml))
            if recurse == True:
                try:
                    if self.subelements:
                        for i in self.subelements:
                            xml.append(i.serialize())
                except AttributeError:
                    pass
            return xml
                    
    def validate(self):
        # Loop through collection of validators
        # ...or just use attrs.  Thanks attrs!
        attr.validate(self)
        if len(self.subelements) > 0:
            for subelement in self.subelements:
                subelement.validate()

@attr.s    
class Mods(ModsObj):
    """A single MODS record"""

    ID = attr.ib(default=None)
    version = attr.ib(default=None)

    # TODO MODS record must have at least 1 subelement
    subelements = attr.ib(factory=list)

class ModsCollection(ModsObj):
    """A MODS collection"""
    # I don't anticipate using this class very much
    
    subelements = attr.ib(factory=list)
            
        
        
    




