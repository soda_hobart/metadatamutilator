from flask import current_app, Blueprint, request, render_template

approutes = Blueprint('approutes', __name__, template_folder='templates')

@approutes.route('/test/<int:recordid>')
def test(recordid):
    return render_template('treeb2.html', recordid=recordid)

@approutes.route('/test2')
def test2():
    return render_template('test2.html')

@approutes.route('/titleInfo')
def titleInfo():
    return render_template('titleInfo.html')


def init_app(app):
    app.register_blueprint(approutes, url_prefix='/app')
