import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)
    app.config.from_mapping(
        SECRET_KEY = os.environ.get('SECRET_KEY') or 'dev',
        DATABASE = os.path.join(app.instance_path, 'mutilator.db'),
        SQLALCHEMY_DATABASE_URI = 'sqlite:////home/sarah/Projects/MetadataMutilator/metadatamutilator/instance/mutilator.db',
        SQLALCHEMY_TRACK_MODIFICATIONS = False
    )

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from mutilator_flask import api, routes
    db.init_app(app)
    api.init_app(app)
    routes.init_app(app)

    return app

def init_db(app):
    from flask_mutilator.Record import Record
    db.create_all(app=create_app())

