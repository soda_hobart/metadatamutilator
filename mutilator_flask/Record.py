from mutilator_flask import db

class Record(db.Model):
    __tablename__ = 'records'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    creator = db.Column(db.String)
    content_xml = db.Column(db.String)
    content_json = db.Column(db.String)


