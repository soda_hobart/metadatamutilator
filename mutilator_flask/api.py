from flask import current_app, Blueprint, request, jsonify
from mutilator_flask.Record import Record
from mutilator_flask import db
from ModsModel import Mods, TitleInfo, StringDefinitions, AttributeGroup
import ModsBuilder
from lxml import etree
import json
import requests

api = Blueprint('api', __name__)

@api.route('/record/<string:recordname>', methods=['GET', 'POST'])
def show_record(recordname):
    record = Record.query.filter_by(name=recordname).first()
    if record.content_json:
        return record.content_json
    else:
        return record.name

@api.route('/record/<int:recordid>', methods=['GET', 'POST'])
def show_recordid(recordid):
    record = Record.query.filter_by(id=recordid).first()
    if request.args.get('format') == 'xml':
        return record.content_xml
    else:
        return record.content_json
        

def int_or_str(var):
    try:
        my_int = int(var)
        return my_int
    except ValueError:
        return var

def establish_uri(record_id, mods_object, path_list, current_index):
    uri_path = 'api/{}'.format(record_id)
    for i in mods_object.subelements:
        pass
        
        
     
@api.route('/record/<int:recordid>/<path:subelementidx>', methods=['GET', 'POST'])
def getter_record_subelements(recordid, subelementidx):
    record = Record.query.filter_by(id=recordid).first()
    d = json.loads(record.content_json)
    mods = ModsBuilder.element_builder(record.content_json)
    print(subelementidx)
    subs = [int_or_str(i) for i in subelementidx.split('/')]
    print(subs)
    def get_from_hierarchy(element, idx):
        counter = 0
        current_elmt = element
        for i in idx:
            if type(i) == int:
                current_elmt = current_elmt.subelements[i]
                counter += 1
                if counter == len(idx):
                    return current_elmt
            elif type(i) == str:
                counter += 1 
                aggregation = [j for j in current_elmt.subelements if j.__class__.mods_name() == i]
                print(aggregation)
                aggregated_elmt = Mods.ModsElementAggregation(subelements=[j for j in aggregation])
                current_elmt = aggregated_elmt
                print(current_elmt)
                if counter == len(idx):
                    return current_elmt
    subelement = get_from_hierarchy(mods, subs)
    print('SUBELEMENT: {}'.format(subelement))
    if request.args.get('format') == 'xml':
        return etree.tostring(subelement.serialize(), pretty_print=True)
    else:
        return subelement.serialize(output='json')
     
                     

@api.route('/record/<int:recordid>/<path:subelementidx>', methods=['GET', 'POST'])
def show_record_subelements(recordid, subelementidx):
     record = Record.query.filter_by(id=recordid).first()
     d = json.loads(record.content_json)
     mods = ModsBuilder.element_builder(record.content_json)
     subs = [int(i) for i in subelementidx.split('/')]
     def get_from_hierarchy(element, idx):
         counter = 0
         current_elmt = element
         for i in idx:
             current_elmt = current_elmt.subelements[i]
             counter += 1
             if counter == len(idx):
                 return current_elmt
     subelement = get_from_hierarchy(mods, subs)
     if request.args.get('format') == 'xml':
         return etree.tostring(subelement.serialize(), pretty_print=True)
     else:
         return subelement.serialize(output='json')

@api.route('/record/add_element/<int:recordid>', methods=['GET', 'POST'])
def add_element(recordid):
    if request.method == 'GET':
        # Need arg with datatype for object to be added--classmethods will produce data to create GUI widgets
        return 'render_template for GUI widgets.  args for POST are resource_uri for parent object, data for child object(subelement).'
    if request.method == 'POST':
        record = db.session.query(Record).filter_by(id=recordid).first()
        d = json.loads(record.content_json)
        print(d)
        mods = ModsBuilder.element_builder(record.content_json)
        parent_obj = list(filter(lambda x: x.resource_uri==request.args.get('targetResource'), mods.iter_all())).pop()
        child_obj = ModsBuilder.element_builder(request.args.get('elementData'))
        parent_obj.add_subelement(child_obj)
        for i in mods.iter_all():
            i.resource_uri = None
            i.parent_resource_uri = None
        ModsBuilder.establish_uri(mods, record.id, 'http://localhost:5000')
        new_json_data = mods.serialize(output='json')
        new_xml_data = etree.tostring(mods.serialize(), pretty_print=True)
        record.content_json = new_json_data
        record.content_xml = new_xml_data
        db.session.commit()
        return record.content_json
     
@api.route('/record/create', methods=['GET', 'POST'])
def create_record():
    if request.method == 'POST':
        if request.args.get('format') == 'json':
            mods_data = request.args.get('data')
            mods_obj = ModsBuilder.element_builder(mods_data)
            print(mods_obj)
            mods_obj.validate()
            # TODO: validate nees to raise errors in a
            # meaningful way; do some normalization, etc.
            kwargs = {'name': request.args.get('name'),
                      'creator': request.args.get('creator'),
                      'content_json': None,
                     # 'content_json': mods_obj.serialize(output='json'),
                      'content_xml': etree.tostring(mods_obj.serialize(), pretty_print=True)
                      }
            record = Record(**kwargs)
            db.session.add(record)
            db.session.flush()
            with_uri = ModsBuilder.establish_uri(mods_obj, record.id, 'http://localhost:5000')
            record.content_json = mods_obj.serialize(output='json')
            db.session.commit()
            return record.content_json
    if request.method == 'GET':
        return "Required POST method params: format(json or xml), data(json_string), creator(string), name(string).  Sorry for the shitty documentation."
                      
                          
     
@api.route('/record/create_test', methods=['GET','POST'])
def create_record_test():
    if request.method == 'POST':
        mods = Mods.Mods()
        mods.add_subelement(TitleInfo.TitleInfo(authority='sammy'))
        mods.subelements[0].add_subelement(TitleInfo.Title(text='Friendliest Pet'))
        kwargs = {'name': request.args.get('name'),
                  'creator': request.args.get('creator'),
                  'content_json': mods.serialize(output='json'),
                  'content_xml': etree.tostring(mods.serialize(output='xml'), pretty_print=True)}
        record = Record(**kwargs)
        db.session.add(record)
        db.session.commit()
        return 'name: {}, creator: {}'.format(record.name, record.creator)
    if request.method == 'GET':
        return "lemme show you how it's done..."

        

def init_app(app):
    app.register_blueprint(api, url_prefix='/api')

