from ModsModel import Mods, TitleInfo
import json

# It's really a director, but the word "Builder" just feels like
# it inspires more confidence...

def module_map():
    mod_map = {'titleInfo': TitleInfo}
    return mod_map


# This fucker rolls a constructor for the Mods class into the
# director for each module's build function.  This seems kind of
# jacked up, and needs to be changed.  Sorry for cussing.
def element_builder(data):
    data = json.loads(data)
    for key, value in data.items():
        if data['datatype'] == 'mods':
            root_mods = Mods.Mods(resource_uri=data['resource_uri'])
            try:
                subs = [i for i in data['subelements']]
                for i in subs:
                    if i['datatype'] in module_map():
                        element = module_map()[i['datatype']].builder(i)
                        root_mods.add_subelement(element)
            except AttributeError:
                print('subelements not present in record, exception ignored')
            return root_mods
        else:
            if data['datatype'] in module_map():
                element = module_map()[data['datatype']].builder(data)
                return element

def establish_uri(mods_obj, record_id, site_location):
    root_uri = '{}/api/record/{}'.format(site_location, record_id)
    mods_obj.resource_uri = root_uri
    def recurse(mods_obj, base_uri):
        try:
            for i in list(enumerate(mods_obj.subelements)):
                i[1].parent_resource_uri = base_uri
                i[1].resource_uri = base_uri + '/{}'.format(i[0])
                recurse(i[1],i[1].resource_uri)
        except AttributeError:
            print('no subelements, exception ignored')
    recurse(mods_obj, root_uri)


if __name__ == '__main__':
    m = Mods.Mods()
    t = TitleInfo.TitleInfo(authority='sammy')
    t.add_subelement(TitleInfo.Title(text='Friendliest Pet'))
    m.add_subelement(t)
    j = m.serialize(output='json')
    #d = json.loads(j)
    e = element_builder(j)
